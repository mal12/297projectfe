import React, { Component } from 'react';
import Background from '../image/background.jpg'
import LogoImage from "../image/logo.png"
import {
    Grid,
    Card,
    CardContent,
    TextField,
    Button,
    Typography,
    Link,
    Paper,
    SvgIcon,
    ThemeProvider,
    Box,
    Divider,
  } from "@material-ui/core";
import {loginUser} from "./api"
const styles = {
    Background:{
        position: "relative",
        top: "0",
        width: "100%",
        height: "100%",
        backgroundSize: "cover",
        backgroundImage: `url(${Background})`,
    },
    Logo: { float: "left", height: 200, width: 350 },
}

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            username: "",
    password: "",
    errors: null,
    usernameError: null,
    passwordError: null,
         }
    }
    handleSubmit = async(event) =>{
      var data = new FormData();
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      event.preventDefault();
      let answer = await loginUser(data);
      if(answer.username){
        console.log(answer.username)
        this.props.setUser(answer.username.toString());
        this.props.history.push("/home");
      }
      else (alert("please enter correct username and password"))
    }
    onFocusField = (attribute) => {
        if (attribute === "username") {
          this.setState({ usernameError: null });
        } else if (attribute === "password") {
          this.setState({ passwordError: null });
        }
      };
    
      onBlurField = (attribute) => {
        if (attribute === "username") {
          if (this.state.username.length === 0) {
            this.setState({
              usernameError: "Please provide a valid username",
            });
          }
        } else if (attribute === "password") {
          if (this.state.password.length === 0) {
            this.setState({ passwordError: "Please provide a password" });
          }
        }
      };

    renderSignInForm = () =>{
        return (<Card>
            <CardContent
              style={{
                marginLeft: "44px",
                marginRight: "44px",
                marginBottom: "44px",
                float: "left",
              }}
            >
              <img src={LogoImage} fullWidth style={styles.Logo} />
              {/* <CardMedia src={LogoImage} component="img" alt="myHealthToday Logo"  wrap style={{ marginBottom: 20 }} /> */}
              
              <Divider style={{marginTop : "13px", marginBottom:"13px"}} fullWidth/>
              <TextField
                variant="outlined"
                margin="dense"
                id="username"
                label="Username"
                fullWidth
                onFocus={() => this.onFocusField("username")}
                onBlur={() => this.onBlurField("username")}
                error={this.state.usernameError ? true : false}
                value={this.state.username}
                onChange={(e) => this.setState({ username: e.target.value })}
              />
              {this.state.usernameError ? (
                <Typography variant="caption" color="error">
                  {" "}
                  {this.state.usernameError}{" "}
                </Typography>
              ) : null}
  
              <TextField
                fullWidth
                variant="outlined"
                margin="dense"
                id="password"
                label="Password"
                type="password"
                style={{ marginTop: "13px" }}
                onFocus={() => this.onFocusField("password")}
                onBlur={() => this.onBlurField("password")}
                error={this.state.passwordError ? true : false}
                value={this.state.password}
                onChange={(e) => this.setState({ password: e.target.value })}
              />
              {this.state.passwordError ? (
                <Typography variant="caption" color="error">
                  {" "}
                  {this.state.passwordError}{" "}
                </Typography>
              ) : null}
  
              <form onSubmit={this.handleSubmit}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  style={{ marginTop: "13px" }}
                  disabled={
                    this.state.password.length === 0 ||
                    this.state.username.length === 0
                  }
                // onClick={() => {
                //   this.handleSubmit();
                // }}
                >
                  Login
                </Button>
              </form>
  
              {this.state.errors ? (
                <div style={{ marginTop: 10 }}>
                  {this.renderErrorMessages()}
                </div>
              ) : null}
              {/* <Box style={{ marginTop: 17 }}>
                <Typography variant="caption"> Forget your password? </Typography>
              </Box> */}
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                style={{
                  marginTop: "13px",
                }}
              >
                <div>
                  <Typography
                    variant="body1"
                    color="primary"
                    style={{ fontSize: "H4" }}
                  >
                    <Link
                      style={{ cursor: "pointer" }}
                      onClick={(e) => {
                        e.preventDefault();
                        this.props.history.push("/password/reset/");
                      }}
                    >
                      Forgot your password?
                    </Link>
                  </Typography>
                </div>
              </Grid>
              <Divider style={{marginTop : "13px", marginBottom:"13px"}} fullWidth/>
              <Button
                  variant="contained"
                  color="primary"
                  fullWidth
                  style={{ marginTop: "44px" }}
                  onClick={(e) => {
                    e.preventDefault();
                    this.props.history.push("/signup");
                  }}
                  // onClick={() => {
                  //   this.handleSubmit();
                  // }}
                >
                  Create New Account
                </Button>
            </CardContent>
          </Card>)
    }
    render() { 
        return ( <div style={styles.Background}> 
            <Grid container direction="row" justify="center" alignItems="center">
          <Grid item lg={6} />
          <Grid item lg={6} xs={12}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              style={{ minHeight: "100vh" }}
            >
              <Grid item style={{ width: "464px", marginRight: "20vm" }}>
                {this.renderSignInForm()}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        </div> );
    }
}
 
export default Login;