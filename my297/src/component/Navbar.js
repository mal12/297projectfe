import React, { Component } from "react";
import logo from "../image/logo.png";
import { useHistory } from "react-router-dom";
import {
  Grid,
  AppBar,
  makeStyles,
  CssBaseline,
  Toolbar,
  Link,
  Box,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    zIndex: theme.zIndex.drawer + 1,
  },
  toolbar: {
    flexWrap: "wrap",
    height: 135,
  },
  toolbarTitle: {
    flexGrow: 2,
  },
  link: {
    // margin: theme.spacing(1, 1.5),
    marginRight: 30,
    color: "#707070",
  },
  lastLink: {
    color: "#707070",
  },
  logo: {
    justifyContent: "flex-start",
  },
}));

function CustomAppbar(context) {
  const classes = useStyles();
  const history = useHistory();
  function Logout() {
    try {
      history.push("/");
    } catch (error) {
      console.log(error.message);
    }
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        style = {{color: "white"}}
        elevation={1}
        className={classes.appBar}
      >
        <Toolbar className={classes.toolbar}>
          <Grid container justify="center" >
            <Grid container justify="center" alignItems="center" style={{width: "90%"}}>
              <Box
                className={classes.toolbarTitle}
                display="flex"
                onClick={() => {
                  history.push("/home");
                }}
              >
                <img
                  src={logo}
                  style={{ height: 100, cursor:"pointer"  }}
                  // marginLeft: "5%"
                  justifycontent="flex-start"
                />
              </Box>

              <nav>
                <Link
                  variant="button"
                  style = {{color: "black"}}
                  href="#"
                  className={classes.link}
                  onClick={() => {
                    history.push("/home");
                  }}
                >
                  Home
                </Link>
                <Link
                  variant="button"
                  style = {{color: "black"}}
                  href="#"
                  className={classes.link}
                  onClick={() => {
                    history.push("/info");
                  }}
                >
                  Infomation
                </Link>
                
                <Link
                  variant="button"
                  style = {{color: "black"}}
                  href="#"
                  className={classes.lastLink}
                  onClick={() => {
                    Logout();
                  }}
                >
                  Logout
                </Link>
              </nav>

             </Grid>

          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}

class Navbar extends Component {
  

  render() {
    const { history } = this.props;
    return (
      <div>
        <CustomAppbar history={this.context} setUser={this.props.setUser} />
      </div>
    );
  }
}

export default Navbar;
