import { CompareArrowsOutlined } from "@material-ui/icons";
import baseUrl from "./rootUrl"
export const getUserInfo = async(e) =>{
    try{
    console.log(e);
    let res = await fetch("/user_info", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "username" : e
      },
    });
    res = await res.json();
    console.log(res);
    return res;
    }
    catch(e){
        console.log(e);
    }
}

export const updateUserInfo = async(username,balance ) =>{
  try{
    var data = new FormData();
    console.log("balance : "  + balance);
    console.log("username : " + username);
    data.append("add_balance", Number(balance));
    console.log(data.get("add_balance"));
  let res = await fetch("/user_info", {
    method: "POST",
    headers: {
      "username" : username
    },
    body:data,
  });
  res = await res.json();
  console.log(res);
  return res;
  }
  catch(e){
      console.log(e);
  }
}

export const updatePassword = async(username,pass ) =>{
  try{
    var data = new FormData();
    data.append("new_password", pass);
    console.log(data.get("add_balance"));
  let res = await fetch("/user_info", {
    method: "POST",
    headers: {
      "username" : username
    },
    body:data,
  });
  res = await res.json();
  console.log(res);
  return res;
  }
  catch(e){
      console.log(e);
  }
}

export const uploadPicture = async(file,username) =>{
    try{
        var data = new FormData();
        data.append("image", file);
        data.append("username", username);
        let res = await fetch("/fruit_categorization", {
            method: "POST",
            body:data
          });
          res = await res.json();
          console.log(res);
          return res;
    }
    catch (e){
        console.log(e);
    }
}

export const loginUser = async (data) =>{
    try{
    let res = await fetch("/login", {
        method: "POST",
        body:data
      });
      console.log(res);
      res = await res.json();
      return res;
    }
    catch (e){
      console.log(e);
  }
}

export const register = async (data) =>{
  try{
  let res = await fetch("/register", {
      method: "POST",
      body:data
    });
    console.log(res);
    res = await res.json();
    return res;
  }
  catch (e){
    console.log(e);
}
}
