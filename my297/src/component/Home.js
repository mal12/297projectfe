import React, { Component } from 'react';
import {getUserInfo, uploadPicture,updateUserInfo} from "./api"
import {
    Grid,Avatar,Typography,Button,Link,Paper, Dialog,DialogTitle,DialogContent,TextField, DialogActions
  } from "@material-ui/core";
import Dropzone from "react-dropzone";
import InsertPhotoOutlinedIcon from '@material-ui/icons/InsertPhotoOutlined';
import Navbar from './Navbar';
import parth from '../image/Parth.png'
const styles = {
    dropZone: {
      width: "100%",
      height: 300,
      border: "2px dashed #DCDCDC",
      marginTop: 20,
      marginBottom: 20,
    },
  };
const website = [
  "AMAZON",
  "TARGET"
]
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            file: null,
            result:null,
            user:null,
            description:null,
            balance:0,
            dialog1:false,
            dialog2:false,
            newBalance:0
         }
    }
    
    componentDidMount = async() =>{
        const user = await getUserInfo(this.props.curUser);
        this.setState({user});
        this.setState({balance : user.balance})
        console.log(user);
    } 
    addFiles = (acceptedFiles) => {
        this.setState({ file: acceptedFiles[0] });
      };
    uploadOnClick = async() =>{
        if(this.state.balance === 0){alert("Your balance is 0, please purchase some new to enjoy the service!")}
        else{
          const result = await uploadPicture(this.state.file, this.props.curUser);
          const user = await getUserInfo(this.props.curUser);
          this.setState({balance : user.balance,result : result, description : result.info, dialog2 : true});
        }
    }
    renderDropZoneContent = () => {
        return (
          <Grid container direction="column" justify="center" alignItems="center"
                style={{marginTop: 20}}>
    
            {this.state.file ? (
                <Avatar
                      variant="rounded"
                      src={URL.createObjectURL(this.state.file)}
                      style={{ width: 200, height: 200 }} />
              ) : 
              <>
                <InsertPhotoOutlinedIcon style={{ width: 60, height: 60, color: "#E2E2E2" }}/>
                <Typography textAlign="center" style={{ color: "#E2E2E2" }}>
                  Drag a photo here or click here to select from your device
                </Typography>
              </>
              }
          </Grid>   
        )
      }
      renderButtons = () => {
        return (
          <>  
            <Grid style={{ marginTop: 150 }}>
              <Button variant="contained" color="primary"
                      style={{ width: 190, height: 40, marginTop: 10, fontSize: 14 }}
                      onClick={() => {this.setState({dialog1 : true})}}
              >
                Buy Service
              </Button>
            </Grid>
            <Grid style={{ marginTop: 20 }}>
            <Button variant="contained" color="primary"
                      style={{ width: 190, height: 40, marginTop: 10, fontSize: 14 }}
                      onClick={() => {this.props.history.push('/info')}}
              >
                Edit Profile
              </Button>
            </Grid>
          </>
        );
      };
    
    onClose1 = () =>{
      this.setState({dialog1 : false});
    }
    onClose2 = () =>{
      this.setState({dialog2 : false});
    }
    render() { 
        return ( <div>
          <Navbar setUser = {this.props.setUser} history={this.props.history} />
            <Grid container direction="row" justify="center" alignItems="center">
              <Grid container direction="row" justify="center" style={{ marginTop: 200, marginBottom: "2vh", width: "90%" }}>
                <Grid item md={12} lg={2}>
                <Grid container direction="column" justify="center" alignItems="center">
                  <Grid style={{ marginLeft: 0 }}>
                    <Avatar
                      src={
                          parth
                      }
                      style={{ width: 150, height: 150, cursor: "pointer" }}
                      
                    ></Avatar>
                  </Grid>
                  {this.renderButtons()}
                </Grid>
              </Grid>
                <Grid item md={12} lg={10}>
                  <Grid style = {{marginTop : 20}}>
                      <Paper variant="outlined" style = {{width : "80%",marginTop : 20, marginBottom : 20,marginLeft:"10%"}}>
                        <Typography>Hi {this.props.curUser}! Welcome to MZC, we are here to provide you
                         best fruit recognition service!</Typography>
                         <br/>
                        <Typography>According to your current account information, you can enjoy {this.state.balance} times service(s).</Typography>
                        <br />
                        <Typography>To enjoy our service you can simply drag or select picture of fruit from you device 
                          to the following drop box and then submit~
                        </Typography>
                      </Paper>
                  </Grid>
                <Dropzone
                onDrop={(acceptedFiles) => this.addFiles(acceptedFiles)}
                accept="image/jpeg,image/jpg,image/png"
                multiple={false}
              >
                {({ getRootProps, getInputProps }) => (
                  <section style={styles.dropZone}>
                    <div {...getRootProps()}>
                      <input {...getInputProps()} />
                        {this.renderDropZoneContent()}
                    </div>
                  </section>
                )}
              </Dropzone>
        <Button variant="contained" color="primary" style={{ minWidth: "40%" }}
                      onClick={async () => {
                        this.uploadOnClick();
                      }}
                    >
                      Submit
            </Button>
            <div>
        
    </div>
                </Grid>
              </Grid>
            </Grid>
            <Dialog open = {this.state.dialog2} onClose={this.onClose2} maxWidth="md"
          fullWidth={true}>
            <DialogTitle>{this.state.result ? this.state.result.category : ""}</DialogTitle>
            <DialogContent>
            {this.state.result && <Grid>
          <Typography>
          This is : {this.state.result.category} . You can view relavant materials in following website:  
          </Typography>
          {
            this.state.result.url.map((link, index) =><div>
              <Link 
              style = {{cursor : "pointer"}}
              onClick={() => {
              window.open(
                link.toString()
              );
            }}>{website[index]}</Link>
            <br />
            </div>)
          }
          <br/>
          <Typography>Here is the detailed description:</Typography>
          <Typography>
          Health Claims: {this.state.description["Health Claims"] ? this.state.description["Health Claims"] : "None"}
          </Typography>
          <br />
          <Typography>
          Nutrient Content Claims: {this.state.description["Nutrient Content Claims"] ? this.state.description["Nutrient Content Claims"] : "None"}
          </Typography>
          <br />
          <Typography>
          Savor: {this.state.description["SAVOR"] ? this.state.description["SAVOR"] : "None"}
          </Typography>
          <br />
          <Typography>
          Store: {this.state.description["STORE"] ? this.state.description["STORE"] : "None"}
          </Typography>
          <br />
          <Typography>
          Select: {this.state.description["select"] ? this.state.description["select"] : "None"}
          </Typography>
          <br />
        </Grid>}
            </DialogContent>
          </Dialog>
        <Dialog open = {this.state.dialog1} onClose={this.onClose1} maxWidth="md"
          fullWidth={true}>
          <DialogTitle>Thank you for your purchase with MZC~</DialogTitle>
          <DialogContent>
          <Typography>Please input the number of service you wanna buy</Typography>
          <TextField
                fullWidth
                variant="outlined"
                margin="dense"
                id="balance"
                label="Balance"
                style={{ marginTop: "13px" }}
                value={this.state.newBalance}
                onChange={(e) => this.setState({ newBalance: e.target.value })}
          />
          </DialogContent>
          <DialogActions>
          <Typography 
              color="primary"
              style={{ cursor: "pointer" }}
              onClick={() => this.setState({ dialog1: false })}
            >
              Cancel
            </Typography>
            <Typography 
              color="primary"
              style={{ cursor: "pointer", marginLeft: 20 }}
              onClick={async () => {
                console.log(this.state.newBalance)
                await updateUserInfo(this.props.curUser,this.state.newBalance);
                this.setState({ dialog1: false});
                this.componentDidMount();
              }}
            >
              Confirm
            </Typography>
          </DialogActions>
        </Dialog>
        </div> );
    }
}
 
export default Home;