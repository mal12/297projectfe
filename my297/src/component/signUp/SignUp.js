import React, { Component } from "react";
import LogoImage from "../../image/logo.png";
import Background from "../../image/background.jpg";

import {
  Grid,
  Card,
  CardContent,
  TextField,
  Button,
  Typography,
  Paper,
  Box,
} from "@material-ui/core";
import { Link } from "react-router-dom";

const styles = {
  Background: {
    position: "relative",
    top: "0",
    width: "100%",
    height: "100%",
    backgroundSize: "cover",
    backgroundImage: `url(${Background})`,
  },
  Logo: {
    float: "left",
    height: 200,
    width: 350,
  },
};

class SignUp extends Component {
  state = {
    username: "",
    password: "",
    confirmpassword: "",
    errors: {
      signup: null,
      username: null,
      password: null,
      confirmpassword: null,
    },
  };

  clearErrorState = () => {
    this.setState({
      errors: {
        signup: null,
        username: null,
        password: null,
        confirmpassword: null,
      },
    });
  };

  handleSubmit = async (event) => {
    this.clearErrorState();

    this.props.setStateValue("stage", Number(1));
    
  };


  onFocusField = (attribute) => {
    const errors = this.state.errors;
    errors[attribute] = null;
    this.setState({ errors });
  };

  onBlurField = (attribute) => {
    switch (attribute) {
      case "username":
        if (this.state.username.length === 0 )
          this.setState({
            errors: {
              ...this.state.errors,
              username: "Please provide a valid username",
            },
          });
        break;
      case "password":
        if (this.state.password.length < 8)
          this.setState({
            errors: { 
              ...this.state.errors, 
              password: "Minimum 8 characters" },
          });
      case "confirmpassword":
        if (this.state.confirmpassword !== this.state.password)
          this.setState({
            errors: {
              ...this.state.errors,
              confirmpassword: "Password value does not match",
            },
          });
        break;
      
      default:
        break;
    }
  };

  checkButtonAbled = () => {

    if (this.state.password.length < 8)
    this.setState({
      errors: { 
        ...this.state.errors, 
        password: "Minimum 8 characters" },
    });

    if (this.state.confirmpassword !== this.state.password)
    this.setState({
      errors: {
        ...this.state.errors,
        confirmpassword: "Password value does not match",
      },
    });

    return (
      !this.state.errors.username &&
      !this.state.errors.password &&
      !this.state.errors.confirmpassword &&
      this.state.username.length > 0 &&
      this.state.password.length >= 8 &&
      this.state.confirmpassword.length >= 8 
    );
  };

  renderSignUpForm = () => {
    return (
      <Card>
        <CardContent
          style={{
            marginTop: "44px",
            marginLeft: "44px",
            marginRight: "44px",
            marginBottom: "44px",
            float: "left",
          }}
        >
          <Box nowrap display="flex">
            <img src={LogoImage} alt="Logo" style={styles.Logo} />
          </Box>

          {/* <CardMedia src={LogoImage} component="img" alt="myHealthToday Logo" style={{marginBottom: 20}}/> */}
          <Box display="block" style={{ marginTop: "44px" }}>
            <Typography style={{ fontSize: "24px", color: "#505050" }}>
              Sign Up with MZC 
            </Typography>
          </Box>
          <form autoComplete="off">
            <TextField
              fullWidth
              variant="outlined"
              margin="dense"
              id="username"
              label="username"
              autoComplete="off"
              style={{ marginTop: "13px" }}
              onFocus={() => this.onFocusField("username")}
              onBlur={() => this.onBlurField("username")}
              error={this.state.errors.email ? true : false}
              value={this.state.email}
              onChange={(e) => {
                this.setState({ username: e.target.value });
                this.props.setStateValue("username", e.target.value)
              }}
            />
            {this.state.errors.username ? (
              <Typography variant="caption" color="error">
                {" "}
                {this.state.errors.username}{" "}
              </Typography>
            ) : null}

            <TextField
              fullWidth
              variant="outlined"
              margin="dense"
              id="password"
              label="Password"
              type="password"
              autoComplete="off"
              style={{ marginTop: "13px" }}
              onFocus={() => this.onFocusField("password")}
              onBlur={() => this.onBlurField("password")}
              error={this.state.errors.password ? true : false}
              value={this.state.password}
              onChange={(e) => {
                this.setState({ password: e.target.value });
                this.props.setStateValue("password", e.target.value)
              }}
            />
            {this.state.errors.password ? (
              <Typography variant="caption" color="error">
                {" "}
                {this.state.errors.password}{" "}
              </Typography>
            ) : null}

            <TextField
              fullWidth
              variant="outlined"
              margin="dense"
              id="confirmpassword"
              label="Re-enter Password"
              type="password"
              autoComplete="off"
              style={{ marginTop: "13px" }}
              onFocus={() => this.onFocusField("confirmpassword")}
              onBlur={() => this.onBlurField("confirmpassword")}
              error={this.state.errors.confirmpassword ? true : false}
              value={this.state.confirmpassword}
              onChange={(e) =>
                this.setState({ confirmpassword: e.target.value })
              }
            />
            {this.state.errors.confirmpassword ? (
              <Typography variant="caption" color="error">
                {" "}
                {this.state.errors.confirmpassword}{" "}
              </Typography>
            ) : null}
          </form>

          <Typography
            style={{ marginTop: "20x", fontSize: "16px", color: "#00000099" }}
          >
            By continuing, you agree to our{" "}
            <a
              href="https://myhealthtoday.care/terms-of-service"
              target="_blank"
              style={{ color: "#F4892C" }}
            >
              Terms of Service
            </a>{" "}
            and{" "}
            <a
              href="https://myhealthtoday.care/privacy-policy"
              target="_blank"
              style={{ color: "#F4892C" }}
            >
              Privacy Policy
            </a>
          </Typography>

          {this.state.errors.signup ? (
            <Paper
              style={{
                backgroundColor: "#F2F2F2 ",
                marginTop: 10,
                display: "inline",
              }}
            >
              <Typography variant="caption">
                {" "}
                {this.state.errors.signup} Please &nbsp;
              </Typography>
              <Link
                variant="caption"
                style={{ color: "#F4892C" }}
                onClick={() => this.props.history.push("/")}
              >
                login in &nbsp;
              </Link>
              <Typography variant="caption">
                with your email and password. If you forget your password,
                please click &nbsp;
              </Typography>
              <Link
                variant="caption"
                style={{ color: "#F4892C" }}
                onClick={() => this.props.history.push("/password/reset/")}
              >
                here
              </Link>
            </Paper>
          ) : null}

          <Button
            variant="contained"
            color="primary"
            fullWidth
            style={{ marginTop: "44px" }}
            // disabled={!this.checkButtonAbled()}
            type="submit"
            onClick={() => {
              if(this.checkButtonAbled()){
                this.handleSubmit();
              }
            }}
          >
            Sign Up
          </Button>
        </CardContent>
      </Card>
    );
  };

  render() {
    return (
      <div style={styles.Background}>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item lg={6} />
          <Grid item lg={6} xs={12}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              style={{ minHeight: "100vh" }}
            >
              <Grid item style={{ width: "464px", marginRight: "20vm" }}>
                {this.renderSignUpForm()}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default SignUp;
