import React, { Component } from "react";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { Typography, Container, Grid } from '@material-ui/core'

class Birthday extends Component {
  dateChange = (date) => {
    this.props.setStateValue("dateOfBirth", date);
  };

  render() {
    return (
      <Container fluid="true" style={{marginTop:'35%'}}>
        <Typography  variant="h4" style={{marginBottom:'5%'}}> {`What is your date of birth?`}</Typography>
        <Grid container justify="center" alignItems="center" style={{marginTop:'20%'}}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              format="MM/dd/yyyy"
              // className={styles.picker}
              value={this.props.birthday}
              onChange={this.dateChange}
            />
          </MuiPickersUtilsProvider>
        </Grid>
      </Container>
    );
  }
}

export default Birthday;
