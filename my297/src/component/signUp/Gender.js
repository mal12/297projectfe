import React, { Component } from "react";
// import { Button } from 'semantic-ui-react'
import { Grid, Typography, Container, Button } from "@material-ui/core";

const classes = {
  selected: {
    border: "1px solid #F4891E",
    color: "#F4891E",
    backgroundColor: "#F4892C27",
    "&:hover, &:focus, &:active": {
      backgroundColor: "#F4892C27",
    },
    minWidth: "15vw",
    minHeight: "5vw",
    marginRight: "1vw",
    fontSize: "30px",
  },
  unselected: {
    border: "1px solid #DDDDDD",
    backgroundColor: "white",
    minWidth: "15vw",
    minHeight: "5vw",
    marginRight: "1vw",
    fontSize: "30px",
  }
};

class Gender extends Component {
  //  state = {
  //    leftActive: true, rightActive: false
  //  }
  //   onClickMale =  () => {
  //     this.props.setStateValue({ gender: "male" });
  //     this.setState((prevState) => ({ leftActive: !prevState.leftActive }))

  //   }
  //   onClickFemale = () => {
  //     // this.props.setStateValue({ gender: "female" });
  //     this.setState((prevState) => ({ rightActive: !prevState.rightActive }))
  //     console.log("clickFemale" + this.state.rightActive)
  //   }

  handleClick = (gender) => {
    this.props.setStateValue("gender", gender);
  };

  render() {
    return (
      <Grid container direction="row" justify="center" alignItems="center" style={{ marginTop: 200 }}>
        <Grid container direction="column" justify="flex-start" alignItems="center" style={{ width: "100" }}>

        <Typography variant="h4">
          What is your biological gender?
        </Typography>
        
        <Grid item style={{ marginTop: 100}}>
          <Button
            style={
              this.props.gender.localeCompare("male") === 0
                ? classes.selected : classes.unselected
            }
            onClick={() => this.handleClick("male")}
          >
            Male
          </Button>
          <Button
            style={
              this.props.gender.localeCompare("female") === 0
                ? classes.selected : classes.unselected
            }
            onClick={() => this.handleClick("female")}
          >
            Female
          </Button>
        </Grid>

      </Grid>
      
    </Grid>

      // <Container fluid="true" style={{ marginTop: "25%" }}>
      //   <Typography variant="h4" style={{ marginBottom: "5%" }}>
      //     {" "}
      //     What is your biological gender?
      //   </Typography>
      //   <Container fluid="true" style={{ marginTop: "20%" }}>
      //     <Button
      //       style={
      //         this.props.gender.localeCompare("male") === 0
      //           ? classes.selected : classes.unselected
      //       }
      //       onClick={() => this.handleClick("male")}
      //     >
      //       Male
      //     </Button>
      //     <Button
      //       style={
      //         this.props.gender.localeCompare("female") === 0
      //           ? classes.selected : classes.unselected
      //       }
      //       onClick={() => this.handleClick("female")}
      //     >
      //       Female
      //     </Button>
      //     {/* <Button toggle active={this.state.leftActive} style={{minWidth: "15vw", minHeight:'5vw',marginRight: "1vw", borderColor:'grey',borderRadius:'5px'}} onClick={() => this.onClickMale()}>
      //         <Typography variant='h5'>Male</Typography>
      //       </Button>
      //       <Button toggle active={this.state.rightActive}  style={{minWidth: "15vw",minHeight:'5vw',borderColor:'grey',borderRadius:'5px'}}  onClick={() => this.onClickFemale()}>
      //         <Typography variant='h5'>Female</Typography>
      //       </Button> */}
      //   </Container>
      // </Container>
    );
  }
}

export default Gender;
