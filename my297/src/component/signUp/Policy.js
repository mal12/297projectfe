import React, { Component } from "react";
import { TextareaAutosize, Typography, Container } from "@material-ui/core";

class Policy extends Component {
  render() {
    return (
      <Container fluid="true" style={{ marginTop: "20%" }}>
        <Typography variant="h4" style={{ marginBottom: "5%" }}>
          Please carefully review our terms of service and privacy policy
        </Typography>
        <Typography >
          Before you create a  account, you need to agree to our {" "}Terms of Service
          {" "} and {" "}
          Privacy Policy
          .
        </Typography>
        {<TextareaAutosize
          aria-label="minimum height"
          style={{ width: "100%", borderColor: "#fff" }}
          rowsMin={20}
          rowsMax={20}
          placeholder="
          If you are entering into this Agreement on behalf of an organization, you represent and warrant that you have the authority to bind such organization. Any person who has access to the Site by virtue of being designated by the Subscriber as an individual user of the Site (an Authorized User) similarly agrees to be bound hereby. If you are entering into this Agreement on behalf of an educational institution, then your Authorized Users are strictly limited to students, faculty and staff; no alumni may be granted access.
      "
        /> }
      </Container>
    );
  }
}

export default Policy;
