import React, { Component } from 'react';
import parth from '../image/Parth.png'
import {
    Grid,Avatar,Typography,CircularProgress, Paper,TextField, Button, Box,Divider
  } from "@material-ui/core";
import {getUserInfo, uploadPicture,updateUserInfo,updatePassword} from "./api"
import Navbar from './Navbar';
class Info extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            user: null,
            balance: 0,
            newBalance:0,
            newPass:"",
            confirmPass:"",
            error : false,
         }
    }
    componentDidMount = async() =>{
        const user = await getUserInfo(this.props.curUser);
        this.setState({user});
        this.setState({balance : user.balance})
    }
    renderLeft = () =>{
        return (
            <Grid>
              <Typography
                style={{ fontSize: "2.0rem", color: "orange", cursor: "pointer" }}
              >
                <Box textAlign="left">
                  Basic Information <br />
                </Box>
              </Typography>
              <Typography
                style={{ fontSize: "1.2rem", marginTop: "10%", cursor: "pointer" }}
                onClick={() => this.setState({ anchor: "personal" })}
              >
                <Box textAlign="left">
                  Buy Balance <br />
                </Box>
              </Typography>
              <Typography
                style={{ fontSize: "1.2rem", marginTop: "10%", cursor: "pointer" }}
                onClick={() => this.setState({ anchor: "contact" })}
              >
                <Box textAlign="left">
                   Change Password<br />
                </Box>
              </Typography>
            </Grid>
          );
    }
    renderPassword = () =>{
        return (<Paper variant="outlined" style = {{width : "80%",marginTop : 20, marginBottom : 20,marginLeft:"10%"}}>
        <Typography>
            Hi {this.props.curUser} ! Welcome to MZC, You can change your password here
        </Typography>
        <br />
        <TextField
            fullWidth
            variant="outlined"
            margin="dense"
            id="newPassword"
            label="newPassword"
            type="password"
            style={{ marginTop: "13px", width : "80%" }}
            value={this.state.newPass}
            onChange={(e) => this.setState({ newPass: e.target.value })}
        />
        <br/>
        <TextField
            fullWidth
            variant="outlined"
            margin="dense"
            id="confirmPassword"
            label="confirmPassword"
            type="password"
            style={{ marginTop: "13px", width : "80%" }}
            value={this.state.confirmPass}
            onChange={(e) => this.setState({ confirmPass: e.target.value })}
        />
        <br/>
        {this.state.error && <Typography variant="caption" color="error">These two passwords are inconsistent</Typography>}
        <br />
        <Button variant="outlined" color="primary" style = {{marginLeft : 50}} onClick={async()=>{
            if(this.state.newPass === this.state.confirmPass){
              var times = this.state.newPass
            await updatePassword(this.props.curUser,times);
            this.setState({newPass : "", confirmPass:"", error : false});
            this.componentDidMount();
            alert("You have successfully reset your password");
            }
            else this.setState({error : true})
        }}>Submit</Button>
        <br />
    </Paper>);
    }
    renderBalance = () =>{
        return (<Paper variant="outlined" style = {{width : "80%",marginTop : 20, marginBottom : 20,marginLeft:"10%"}}>
        <Typography>
            Hi {this.props.curUser} ! Welcome to MZC, your sincere friend! Your left balance(s) number is {this.state.balance}.
            <br />
            Wanna enjoy more? Enter the balance number you want purchase in the following input box
        </Typography>
        <br />
        <TextField
            fullWidth
            variant="outlined"
            margin="dense"
            id="balance"
            label="Balance"
            style={{ marginTop: "13px", width : "80%" }}
            value={this.state.newBalance}
            onChange={(e) => this.setState({ newBalance: e.target.value })}
        />
        <br />
        <Button variant="outlined" color="primary" style = {{marginLeft : 50}} onClick={async()=>{
            var times = this.state.newBalance
            await updateUserInfo(this.props.curUser,times);
            this.setState({newBalance : 0});
            this.componentDidMount();
        }}>Submit</Button>
        <br />
    </Paper>)
    }
    render() { 
        return ( <div>
            <Navbar setUser = {this.props.setUser} history={this.props.history} />
            <Grid container direction="row" justify="center" alignItems="center">
              <Grid
                container
                direction="row"
                justify="space-between"
                style={{ marginTop: 200, width: "90%" }}
              >
                <Grid
                  item
                  md={2}
                  direction="column"
                  justify="flex-start"
                  alignItems="flex-start"
                >
                  {this.renderLeft()}
                </Grid>
                <Grid item md={1} />
                <Grid
                  item
                  md={9}
                  direction="column"
                  justify="flex-start"
                  alignItems="flex-start"
                >
                  <Grid
                    style={{
                      left: "5%",
                      width: "15%",
                      marginTop: "1%",
                      marginBottom: "3%",
                    }}
                  >
                    <Avatar
                      src={
                          parth
                      }
                      style={{ width: 150, height: 150, cursor: "pointer" }}
                      
                    ></Avatar>
                  </Grid>
                  {this.state.user ? this.renderBalance() : <CircularProgress />}
                  <Divider style={{ marginTop: "5%" }} />
                  {this.state.user ? this.renderPassword() : <CircularProgress />}
                </Grid>
              </Grid>
            </Grid>
            
          </div> );
    }
}
 
export default Info;