import React, { Component } from "react";

import { LinearProgress, Grid, Button, Typography } from "@material-ui/core";

import Birthday from "./signUp/Birthday";
import Gender from "./signUp/Gender";
import Policy from "./signUp/Policy";
import SignUp from "./signUp/SignUp"
import {register} from "./api"
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stage: Number(0),
      dateOfBirth: new Date(),
      gender: "Male",
      username:"",
      password:"",
    };
  }
  setStateValue = (key, value) => {
    this.setState({ [key]: value });
  };
  getMyDate=(e)=>{
    let year = e.getFullYear()
    let month = e.getMonth() + 1
    let day = e.getDate()
    if (month < 10) month = '0' + month
    if (day < 10) day = '0' + day
    return month + '-' + day + '-' +year
}
  handleRegister = async() =>{
    var data = new FormData();
      data.append("username", this.state.username);
      data.append("password", this.state.password);
      data.append("dob", this.state.dateOfBirth);
      data.append("gender", this.state.gender);
    const res = await register(data);
    if(res.error){alert(res.error)}
    else this.props.history.push('/')
  }
  renderSurvery = () => {
    switch (this.state.stage) {
      // case Number(1):
      //   return <Policy />;
      case Number(4):
        return (
          <Birthday
            setStateValue={this.setStateValue}
            username={this.state.username}
            birthday={this.state.dateOfBirth}
          />
        );
      case Number(2):
        return (
          <Gender
            setStateValue={this.setStateValue}
            gender={this.state.gender}
          />
        );
      case Number(1):
        return(
          <Policy setStateValue={this.setStateValue} history={this.props.history}/>
        )
      default:
        break;
    }
  };

  renderButton = () => {
    if (this.state.stage === Number(4)) {
      return (
        <div style={{ marginBottom: "5vh" }}>
          <Button
            variant="contained"
            color="primary"
            style={{ minWidth: "15vw" }}
            onClick={() => this.setState({ stage: this.state.stage + 1 })}
          >
            Next
          </Button>
        </div>
      );
    }
    let leftButtonText = "Back",
        rightButtonText = "Save & Next";
    let leftButtonAction = () => this.setState({ stage: this.state.stage - 1 });
    let rightButtonAction = () =>
      this.setState({ stage: this.state.stage + 1 });
    if(this.state.stage === Number(1)){
      rightButtonText = "Accept";
      rightButtonAction = () =>{
        this.handleRegister(this);
      }
    }

    return (
      <div style={{ marginBottom: "5vh" }}>
        <Button
          variant="contained"
          color="secondary"
          style={{ minWidth: "15vw", marginRight: "1vw" }}
          onClick={() => leftButtonAction()}
        >
          {leftButtonText}
        </Button>
        <Button
          variant="contained"
          color="primary"
          style={{ minWidth: "15vw" }}
          onClick={() => rightButtonAction()}
        >
          {rightButtonText}
        </Button>
      </div>
    );
  };

  renderComponent = () => {
    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        style={{ minHeight: "100vh" }}
      >
        <Grid item xs={12}>
          <Grid
            container
            direction="column"
            justify="space-around"
            alignItems="center"
            style={{ minHeight: "100vh" }}
          >
            <Grid style={{ height: "80vh", marginBottom: "44px" }}>
              {this.renderSurvery()}
            </Grid>

            <Grid style={{ height: "20vh", marginTop: "44px" }}>
              <Grid
                container
                direction="column"
                justify="space-around"
                alignItems="center"
              >
                {this.renderButton()}
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  <LinearProgress
                    color="primary"
                    variant="determinate"
                    value={(this.state.stage * 100) / 2}
                    style={{ width: "31vw" }}
                  />
                </Grid>
                <Typography variant="subtitle1">
                  {`Step ${this.state.stage}`}{" "}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  };

  render() {
    if(this.state.stage === 0)return <SignUp history={this.props.history} setStateValue={this.setStateValue}></SignUp>
    else return <div>{this.renderComponent()}</div>;
  }
}

export default Register;
