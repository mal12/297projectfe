import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import Login from './component/Login'
import Register from './component/Register'
import Home from './component/Home'
import Info from './component/Info'
class App extends Component {

 state={
   curUser:null,
 };
 setUser =(user) =>{
   console.log(user);
   this.setState({curUser:user});
   console.log(this.state.curUser);
 }
 render() {
  return (
    <div className="App">
      <Router>
        <Switch>
        <Route exact path = "/" render={(props) => <Login setUser = {this.setUser} curUser = {this.state.curUser}{...props} />} />
        <Route exact path = "/signup" render={(props) => <Register {...props} curUser = {this.state.curUser}/>} />
        <Route exact path = "/home" render={(props) => <Home {...props} setUser = {this.setUser} curUser = {this.state.curUser} />} />
        <Route exact path = "/info" render={(props) => <Info {...props} setUser = {this.setUser} curUser = {this.state.curUser} />} />
        </Switch>
      </Router>
    </div>
  );
 }
}

export default App;
